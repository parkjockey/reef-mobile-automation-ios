package us.reef.mobile.automation.utils;

import lombok.experimental.UtilityClass;

import java.security.SecureRandom;
import java.util.Random;

@UtilityClass
public class RegisterNewUserUtil {
    /**
     * Get random email
     *
     * @return email
     */
    public String getRandomEmail() {
        return "reef.mobile.automation+" + new Random().nextInt(99999999) + "@gmail.com";
    }

    /**
     * Get random password
     *
     * @param length length of password
     * @return password
     */
    public String getRandomPassword(final int length) {
        final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        final String NUMBER = "0123456789";
        final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
        final SecureRandom random = new SecureRandom();

        final StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            final int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            final char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            sb.append(rndChar);
        }
        return "As3" + sb.toString();
    }

    /**
     * Generate random mobile phone number
     *
     * @return Mobile Phone Number
     */
    public String generateRandomMobilePhoneNumber() {
        final int randomFirstNumber = new Random().nextInt(999 - 100) + 100;
        final int randomSecondNumber = new Random().nextInt(9999 - 1000) + 1000;
        return "(555) " + randomFirstNumber + "-" + randomSecondNumber;
    }

    /**
     * Generate random mobile phone number on sign up
     *
     * @return Mobile Phone Number
     */
    public String generateRandomMobilePhoneNumberOnSignUp() {
        final int randomFirstNumber = new Random().nextInt(999 - 100) + 100;
        final int randomSecondNumber = new Random().nextInt(9999 - 1000) + 1000;
        return "555" + randomFirstNumber + "" + randomSecondNumber;
    }
}