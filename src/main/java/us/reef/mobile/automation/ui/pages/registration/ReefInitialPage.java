package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ReefInitialPage extends BasePage {

    final By logInButton = MobileBy.AccessibilityId("Log in");
    final By signUpButton = MobileBy.AccessibilityId("Sign up");

    public ReefInitialPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isReefInitialPageLoaded() {
        log.info("Check that Reef initial landing page is displayed.");
        return waitForElementToBeDisplayed(signUpButton) || waitForElementToBeDisplayed(logInButton);
    }

    public void clickSignUpButton() {
        waitAndClick(signUpButton);
        log.info("User clicked sign up button");
    }

    public void clickLogInButton() {
        waitAndClick(logInButton);
        log.info("User clicked log in button.");
    }
}