package us.reef.mobile.automation.ui.pages.popup;

import org.awaitility.core.ConditionTimeoutException;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PopUpPage extends BasePage {

    final By sendNotificationsPopUp = By.xpath("//XCUIElementTypeStaticText[@name=\"“REEF Mobile - UAT” Would Like to Send You Notifications\"]");
    final By allowSendingNotificationPopUpButton = MobileBy.AccessibilityId("Allow");

    final By rateAppPopUp = By.xpath("//*[@name='Enjoying REEF Mobile - UAT?']");
    final By rateAppNotNowButton = By.xpath("//*[@name='Not Now']");

    public PopUpPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public void allowLocationAccess() {
        final String accessYourLocation = "//*[@value='Allow “REEF Mobile - UAT” to access your location?']";
        final String useYourLocation = "//*[@value='Allow “REEF Mobile - UAT” to use your location?']";
        final String accessLocationWhileUsingTheApp = "//*[@value='Allow “REEF Mobile - UAT” to access your location while you are using the app?']";
        final String allow = "Allow";
        final String allowWhileUsingApp = "Allow While Using App";
        final String alwaysAllow = "Always Allow";
        if (isLocationNotificationPopUpDisplayed()) {
            if (isElementPresentAndDisplayed(By.xpath(accessYourLocation))
                    || isElementPresentAndDisplayed(By.xpath(useYourLocation))
                    || isElementPresentAndDisplayed(By.xpath(accessLocationWhileUsingTheApp))) {
                if (isElementPresentAndDisplayed(MobileBy.AccessibilityId(alwaysAllow))) {
                    driver.findElement(MobileBy.AccessibilityId(alwaysAllow)).click();
                } else if (isElementPresentAndDisplayed(MobileBy.AccessibilityId(allowWhileUsingApp))) {
                    driver.findElement(MobileBy.AccessibilityId(allowWhileUsingApp)).click();
                } else if (isElementPresentAndDisplayed(MobileBy.AccessibilityId(allow))) {
                    driver.findElement(MobileBy.AccessibilityId(allow)).click();
                }
            }
            log.info("Agreed to Allow location access.");
        }
    }

    public boolean isLocationNotificationPopUpDisplayed() {
        final String accessYourLocation = "//*[@value='Allow “REEF Mobile - UAT” to access your location?']";
        final String useYourLocation = "//*[@value='Allow “REEF Mobile - UAT” to use your location?']";
        final String accessLocationWhileUsingTheApp = "//*[@value='Allow “REEF Mobile - UAT” to access your location while you are using the app?']";
        try {
            Awaitility.waitAtMost(60, TimeUnit.SECONDS)
                    .with().pollDelay(6, TimeUnit.SECONDS)
                    .and().with().pollInterval(6, TimeUnit.SECONDS)
                    .until(() -> isElementPresentAndDisplayed(By.xpath(accessYourLocation))
                            || isElementPresentAndDisplayed(By.xpath(useYourLocation))
                            || isElementPresentAndDisplayed(By.xpath(accessLocationWhileUsingTheApp)));
        } catch (ConditionTimeoutException e) {
            return false;
        }
        return true;
    }

    public void waitForNotificationsPopUpToBeDisplayedAndAllowIt() {
        log.info("Waiting for notification pop up to be displayed and allow it.");
        waitForElementToBeDisplayed(sendNotificationsPopUp);
        waitAndClick(allowSendingNotificationPopUpButton);
        log.info("Allowed sending of the notifications.");
    }

    public boolean isRateAppNotificationDisplayed() {
        log.info("Checking is Rate App notification shown.");
        try {
            Awaitility.waitAtMost(20, TimeUnit.SECONDS)
                    .with().pollDelay(2, TimeUnit.SECONDS)
                    .and().with().pollInterval(2, TimeUnit.SECONDS)
                    .until(() -> isElementPresentAndDisplayed(rateAppPopUp));
        } catch (ConditionTimeoutException e) {
            return false;
        }
        return true;
    }

    public void tapRateAppNotNowButton() {
        log.info("Tapped not now button on rate app notification pop up.");
        waitAndClick(rateAppNotNowButton);
    }
}
