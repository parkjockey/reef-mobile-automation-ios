package us.reef.mobile.automation.ui.base;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.offset.ElementOption;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionTimeoutException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.TimeUnit;

@Slf4j
@Getter
public abstract class BasePage {

    @Value("${bs.osVersion}")
    public String osVersion;

    @Value("${bs.mobilePhone}")
    public String mobilePhone;

    protected final IOSDriver<IOSElement> driver;
    private final WebDriverWait webDriverWait;

    public BasePage(final BaseDriver baseDriver) {
        this.driver = baseDriver.getDriver();
        this.webDriverWait = new WebDriverWait(driver, 30);
    }

    /**
     * Wait for element to be displayed and send keys to it
     *
     * @param by         element on the page
     * @param keysToSend value which we send to the element
     */
    public void waitAndSendKeys(final By by, final String keysToSend) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).sendKeys(keysToSend);
    }

    /**
     * Wait for element to be displayed and click on it
     *
     * @param by element to be waited for and clicked
     */
    public void waitAndClick(final By by) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).click();
    }

    /**
     * Wait for element to be displayed
     *
     * @param by element to be waited for
     * @return true if element is displayed otherwise false
     */
    public boolean waitForElementToBeDisplayed(final By by) {
        return getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed();
    }

    /**
     * Wait if element is going to be displayed on the screen after some time
     *
     * @param by       element which is being searched
     * @param timeUnit time unit
     * @param duration duration
     * @return false if element is not displayed on the page, otherwise true
     */
    public boolean waitIfElementIsGoingToBeVisibleAfterSomeTime(final By by, final TimeUnit timeUnit, final int duration) {
        try {
            Awaitility.given().atMost(duration, timeUnit)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(2, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(by).isDisplayed());
        } catch (final ConditionTimeoutException | NoSuchElementException e) {
            return false;
        }
        return true;
    }

    /**
     * Check is element present and displayed on the page
     *
     * @param by element to bee checked
     * @return true if it is displayed and present otherwise false
     */
    public boolean isElementPresentAndDisplayed(final By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (final NoSuchElementException e) {
            return false;
        }
    }


    /**
     * Check is element enabled on the page
     *
     * @param by element to bee checked
     * @return true if it is enabled otherwise false
     */
    public boolean isElementEnabled(final By by) {
        try {
            return driver.findElement(by).isEnabled();
        } catch (final NoSuchElementException e) {
            return false;
        }
    }

    public static void longPressAndMoveTo(final IOSDriver<IOSElement> driver, final By element1, final By element2) {
        final TouchAction<?> dragNDrop = new TouchAction<>(driver);
        dragNDrop.longPress(element(driver.findElement(element1)))
                .moveTo(element(driver.findElement(element2)))
                .release();
        dragNDrop.perform();
    }

    /**
     * This method creates a build instance of the {@link ElementOption}.
     *
     * @param element is the element to calculate offset from.
     * @return the built option
     */
    public static ElementOption element(final WebElement element) {
        return new ElementOption().withElement(element);
    }
}