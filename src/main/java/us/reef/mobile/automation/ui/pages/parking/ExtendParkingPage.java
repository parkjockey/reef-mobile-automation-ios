package us.reef.mobile.automation.ui.pages.parking;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ExtendParkingPage extends BasePage {

    @Autowired
    private Storage storage;

    final By extendParkingPageId = By.xpath("//*[@name='Buy Parking']");
    final By lotName = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]");
    final By vehicleName = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]");
    final By creditCard = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeStaticText[2]");
    final By originalSessionTime = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[4]/XCUIElementTypeStaticText[2]");
    final By newEndTime = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[5]/XCUIElementTypeStaticText[2]");
    final By extendNowButton = MobileBy.AccessibilityId("GetQuoteScreen_BuyButton");
    final By extendNowButtonText = By.xpath("//*/XCUIElementTypeButton[1]/XCUIElementTypeStaticText");

    public ExtendParkingPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isExtendParkingPageDisplayed() {
        return waitForElementToBeDisplayed(extendParkingPageId);
    }

    public String getVehiclePlate() {
        return driver.findElement(vehicleName).getText();
    }

    public String getCreditCard() {
        return driver.findElement(creditCard).getText();
    }

    public String getOriginalSessionTime() {
        return driver.findElement(originalSessionTime).getText();
    }

    public String getExtendUntilTime() {
        return driver.findElement(newEndTime).getText();
    }

    public String getLotName () { return driver.findElement(lotName).getText(); }

    public void checksThatExtendEndTimeIsCorrect(final int hours, final int minutes) {
        if (driver.findElement(originalSessionTime).getText().contains("AM") ||
                driver.findElement(originalSessionTime).getText().contains("PM")) {
            final String format = "h':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(newEndTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(hours, minutes,0, DateTimeFormatter.ofPattern(format)))
                    || driver.findElement(newEndTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(hours, minutes,1, DateTimeFormatter.ofPattern(format))));
        } else {
            final String format = "k':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(newEndTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(hours, minutes,0, DateTimeFormatter.ofPattern(format)))
                    || driver.findElement(newEndTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(hours, minutes,1, DateTimeFormatter.ofPattern(format))));
        }
        final ParkingEntity parkingEntity = ParkingEntity.builder()
                .selectedHours(hours)
                .selectedMinutes(minutes)
                .build();

        storage.getParkingEntities().add(parkingEntity);
        log.info("End time of parking duration is correct.");
    }

    public boolean isExtendNowButtonEnabled() {
        log.info("Checking is Extend Now button displayed.");
        return isElementEnabled(extendNowButton);
    }

    public void clickExtendNowButton() {
        waitForElementToBeDisplayed(extendNowButton);
        Awaitility.await()
                .atMost(30, TimeUnit.SECONDS)
                .pollDelay(5, TimeUnit.SECONDS)
                .pollInterval(5, TimeUnit.SECONDS)
                .until(() -> !driver.findElement(extendNowButtonText).getText().isEmpty());
        final String extendNowButtonValue = driver.findElement(extendNowButtonText).getText();
        log.info("Extend now button has text: {}", extendNowButtonValue);
        final String totalExtendParkingAmount = extendNowButtonValue.replaceAll("\\w+ \\w+: ", "");
        setExtendParkingInformation(totalExtendParkingAmount);
        waitAndClick(extendNowButton);
        log.info("Tapped on {} button.", extendNowButtonValue);
    }

    private void setExtendParkingInformation(final String totalExtendParkingAmount) {
        final String lotNameTitle = driver.findElement(lotName).getText();
        final String vehicle = driver.findElement(vehicleName).getText();
        storage.getLastParkingEntity().setLotName(lotNameTitle);
        storage.getLastParkingEntity().setVehicle(vehicle);
        storage.getLastParkingEntity().setEndTime(ParkingTimeUtil.getExtendParkingExpectedEndTime());
        storage.getLastParkingEntity().setStartTime(ParkingTimeUtil.getBuyParkingInitialStartTime());
        if (totalExtendParkingAmount.contains("$")) {
            storage.getLastParkingEntity().setTotalParkingAmount(totalExtendParkingAmount);
        } else {
            storage.getLastParkingEntity().setTotalParkingAmount("$0");
        }
    }
}
