package us.reef.mobile.automation.ui.pages.account;

import io.appium.java_client.ios.IOSElement;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehiclesPage extends BasePage {

    @Autowired
    private Storage storage;

    final By licensePlatePageId = MobileBy.AccessibilityId("Add your license plate");
    final By vehiclesPageId = By.xpath("//*[@name='Vehicles']");
    final By addVehiclePageId = By.xpath("//*[@name='Add Vehicle']");
    final By vehiclePlateNumberFiled = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField");
    final By stateDropDown = MobileBy.AccessibilityId("Select a state/province");
    final By stateDropDownNewVehicle = By.xpath("//*[@name='Select a state/province']");
    final By addLicensePlateButton = By.xpath("//*[@name='Add License Plate']");
    final By stateWheel = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypePicker/XCUIElementTypePickerWheel");
    final By stateWheelNewVehicle = By.xpath("//*[@type='XCUIElementTypePickerWheel']");
    final By skipThisStepButton = MobileBy.AccessibilityId("Skip this step");
    final By addVehicleButton = MobileBy.AccessibilityId("Add");
    final By vehiclesFromList = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
    final By licensePlateNumberField = By.xpath("//*[@value='License plate number']");
    final By navigateBackButton = MobileBy.AccessibilityId("Account");


    public VehiclesPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isLicensePlatePageDisplayed() {
        return waitForElementToBeDisplayed(licensePlatePageId);
    }

    public boolean isVehiclesPageDisplayed() {
        log.info("Checking that Vehicles page is displayed.");
        return waitForElementToBeDisplayed(vehiclesPageId);
    }

    public boolean isAddLicensePlatePageDisplayed() {
        log.info("Checking that Add License Plate page is displayed.");
        return waitForElementToBeDisplayed(addVehiclePageId);
    }

    public void enterLicensePlateNumber(final String licensePlateNumber) {
        waitAndClick(vehiclePlateNumberFiled);
        waitAndSendKeys(vehiclePlateNumberFiled, licensePlateNumber);
        log.info("Entered license plate {}", licensePlateNumber);
        final List<String> licensePlateNumbers = new ArrayList<>();
        licensePlateNumbers.add(licensePlateNumber);
        storage.getLastUserEntity().setLicensePlates(licensePlateNumbers);
    }

    public void enterLicensePlateNumberForNewVehicle(final String licensePlateNumber) {
        waitAndSendKeys(licensePlateNumberField, licensePlateNumber);
        if (storage.getLastUserEntity().getLicensePlates() == null) {
            final List<String> licensePlateNumbers = new ArrayList<>();
            licensePlateNumbers.add(licensePlateNumber);
            log.info("Entered license plate number: " + licensePlateNumber);
            storage.getLastUserEntity().setLicensePlates(licensePlateNumbers);
        } else {
            log.info("Entered license plate number: " + licensePlateNumber);
            storage.getLastUserEntity().getLicensePlates().add(licensePlateNumber);
        }
    }

    public void selectStateFromWheel(final String state) {
        waitAndClick(stateDropDown);
        waitAndSendKeys(stateWheel, state);
    }

    public void selectStateFromWheelForNewVehicle(final String state) {
        waitAndClick(stateDropDownNewVehicle);
        final List<String> states = new ArrayList<>();
        states.add(state);
        storage.getLastUserEntity().setStates(states);
        log.info("State is: {}", state.substring(5));
        waitAndSendKeys(stateWheelNewVehicle, state.substring(5));
    }

    public void clickOnSkipThisStepButton () {
        waitAndClick(skipThisStepButton);
    }

    public void clickOnAddLicensePlateButton() {
        waitAndClick(addLicensePlateButton);
        log.info("Tapped on add license plate button.");
    }

    public void clickAddVehicleButton() {
        waitForElementToBeDisplayed(addVehicleButton);
        waitAndClick(addVehicleButton);
        log.info("Taps on Add vehicle button.");
    }

    public List<IOSElement> getListOfVehicles() {
        waitForElementToBeDisplayed(vehiclesFromList);
        return driver.findElements(vehiclesFromList);
    }

    public boolean isVehiclePresentInTheList(final String vehicle) {
        return waitForElementToBeDisplayed(By.xpath("//*[@name='"+vehicle+"']"));
    }

    public boolean isFirstVehiclePrimary() {
        return getListOfVehicles().stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First vehicle not found!"))
                .findElement(By.xpath("//*[@name='Primary']")).isDisplayed();
    }

    public void clickNavigateToAccountButton() {
        waitAndClick(navigateBackButton);
    }
}