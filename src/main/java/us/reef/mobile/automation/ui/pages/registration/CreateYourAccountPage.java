package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreateYourAccountPage extends BasePage {

    final By createYourAccountPageId = MobileBy.AccessibilityId("Create your account");
    final By emailField = By.xpath("//*[@value='Email']");
    final By passwordField = By.xpath("//*[@value='Password']");
    final By phoneField = By.xpath("//*[@value='Phone']");
    final By licenseAgreementSwitch = By.xpath("//*[@value='0']");
    final By createAccountButton = MobileBy.AccessibilityId("Create Account");

    public CreateYourAccountPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isCreateYourAccountPageDisplayed() {
        log.info("Checking is create your account page displayed.");
        return waitForElementToBeDisplayed(createYourAccountPageId);
    }

    public void enterEmail(final String email) {
        waitAndSendKeys(emailField, email);
    }

    public void enterPassword(final String password) {
        waitAndSendKeys(passwordField, password);
    }

    public void enterMobilePhone(final String mobilePhone) {
        waitAndSendKeys(phoneField, mobilePhone);
    }

    public void checkLicenseAgreementSwitch() {
        waitAndClick(licenseAgreementSwitch);
        log.info("User Agrees to License Agreement.");
    }

    public void tapCreateAccountButton() {
        waitAndClick(createYourAccountPageId);
        waitAndClick(createAccountButton);
        log.info("User taps on Create Account button.");
    }
}
