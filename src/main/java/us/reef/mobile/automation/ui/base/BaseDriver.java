package us.reef.mobile.automation.ui.base;

import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import static io.appium.java_client.remote.MobileCapabilityType.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;

@Slf4j
@Component
@Setter
@Getter
@Scope("cucumber-glue")
public class BaseDriver {

    @Autowired
    private Storage storage;

    @Value("${bs.buildVersion}")
    private String buildVersion;

    @Value("${bs.appUrl}")
    private String appUrl;

    @Value("${bs.mobilePhone}")
    private String mobilePhone;

    @Value("${bs.osVersion}")
    private String osVersion;

    private static final String PLATFORM = "iOS";

    private boolean useRemoteWebDriver;
    private String remoteWebDriverUrl = null;
    private String localWebDriverUrl = null;

    private DesiredCapabilities capabilities = new DesiredCapabilities();

    private final ThreadLocal<IOSDriver<IOSElement>> driver = new ThreadLocal<>();

    /**
     * Setup method with selenium grid
     * @param grid Selenium Grid, possible values "none", "local", "remote" (case insensitive)
     */
    public BaseDriver(@Value("${grid}") final String grid) {
        if (getGrid(grid).equals(Grid.NONE) || getGrid(grid).equals(Grid.LOCAL)) {
            this.useRemoteWebDriver = false;
            this.localWebDriverUrl = Grid.LOCAL.url;
        } else {
            this.useRemoteWebDriver = true;
            this.remoteWebDriverUrl = Grid.REMOTE.url;
        }
    }

    /**
     * Get driver
     *
     * @return {@link EventFiringWebDriver}
     */
    public IOSDriver<IOSElement> getDriver() {
        return driver.get();
    }

    /**
     * Initialize WebDriver
     */
    public void initializeWebDriver() {
        if (this.useRemoteWebDriver) {
            log.info("Using Remote WebDriver");
            setRemoteDesiredCapabilities();
            try {
                log.info("Initializing remote WebDriver with url {} and for platform {}.", remoteWebDriverUrl, PLATFORM);
                driver.set(new IOSDriver<>(new URL(remoteWebDriverUrl), getCapabilities()));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing remote WebDriver with BrowserStack: " + remoteWebDriverUrl;
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        } else {
            setLocalDesiredCapabilities();
            log.info("Initializing driver.");
            try {
                driver.set(new IOSDriver<>(new URL(localWebDriverUrl), getCapabilities()));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing local WebDriver with url: " + localWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        }
    }

    /**
     * Get Application file
     * @return application file {@link File}
     */
    private File getAppFile() {
        try {
            return Paths.get(Objects.requireNonNull(getClass()
                    .getClassLoader().getResource("app/REEF Mobile.app.zip"))
                    .toURI()).toFile();
        } catch (URISyntaxException e) {
            throw new MobileAutomationException("App path could not be found, please check! {}", e.toString());
        }
    }

    /**
     * Set Local Desired Capabilities
     */
    private void setLocalDesiredCapabilities() {
        capabilities.setCapability(PLATFORM_NAME, "iOS");
        capabilities.setCapability(DEVICE_NAME, mobilePhone);
        capabilities.setCapability(PLATFORM_VERSION, osVersion);
        capabilities.setCapability(AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(FULL_RESET, "true");
        capabilities.setCapability(APP, String.valueOf(getAppFile()));
    }

    /**
     * Set remote desired capabilities
     */
    private void setRemoteDesiredCapabilities() {
        final String appiumVersion;
        if (osVersion.contains("12") || osVersion.contains("13") || osVersion.contains("14")) {
            appiumVersion = "1.18.0";
        } else if (osVersion.contains("10")) {
            appiumVersion = "1.15.0";
        } else {
            appiumVersion = "1.16.0";
        }
        capabilities.setCapability("device", mobilePhone);
        capabilities.setCapability("os_version", osVersion);
        capabilities.setCapability("browserstack.appium_version", appiumVersion);
        capabilities.setCapability("bundleId", "com.reef.reef1");
        capabilities.setCapability("browserstack.gpsLocation", "42.5713512,-71.8022492");
        capabilities.setCapability("browserstack.networkLogs", "true");
        capabilities.setCapability("includeNonModalElements", "true");
        capabilities.setCapability("locationServicesEnabled", "true");
        capabilities.setCapability("locationServicesAuthorized", "true");
        capabilities.setCapability("realDevice", "true");
        capabilities.setCapability("project", "iOS - Reef Mobile Automation");
        capabilities.setCapability("build", buildVersion);
        capabilities.setCapability("name", storage.getScenarioEntity().getScenarioName());
        capabilities.setCapability("app", appUrl);
    }

    private Grid getGrid(@Nullable final String grid) {
        if (!StringUtils.isBlank(grid)) {
            return Grid.valueOf(grid.toUpperCase());
        } else {
            return Grid.NONE;
        }
    }

    /**
     * Tear down driver
     */
    public void tearDown() {
        log.info("Closing Driver for platform: {}.", PLATFORM);
        if (null != getDriver()) {
            getDriver().quit();
            driver.remove();
            log.info("Driver closed.");
        } else {
            throw new MobileAutomationException("In tearDown, driver is null!");
        }
    }
}