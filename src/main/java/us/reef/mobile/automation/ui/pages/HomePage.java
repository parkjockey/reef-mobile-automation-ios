package us.reef.mobile.automation.ui.pages;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class HomePage extends BasePage {

    final By homePageId = MobileBy.AccessibilityId("REEF_Mobile.MapView");
    final By mapSection = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther");
    final By accountButton = MobileBy.AccessibilityId("Account");
    final By recentLotsButton = MobileBy.AccessibilityId("Recent Lots");

    public HomePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isHomePageDisplayed() {
        log.info("Checking that Home Page is displayed.");
        return waitForElementToBeDisplayed(homePageId);
    }

    public boolean isMapDisplayed() {
        log.info("Checking is Map displayed.");
        return driver.findElement(mapSection).isDisplayed();
    }

    public void clickAccountButton() {
        waitAndClick(accountButton);
        log.info("User tapped on account button.");
    }

    public void clickRecentLotsButton() {
        waitAndClick(recentLotsButton);
        log.info("Tapped on Recent Lots button.");
    }
}