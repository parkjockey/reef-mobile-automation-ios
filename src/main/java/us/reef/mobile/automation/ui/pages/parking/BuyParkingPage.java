package us.reef.mobile.automation.ui.pages.parking;

import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class BuyParkingPage extends BasePage {

    @Autowired
    private Storage storage;

    final By buyParkingPageId = By.xpath("//XCUIElementTypeNavigationBar[@name=\"Buy Parking\"]");
    final By requestRateButton = MobileBy.AccessibilityId("GetQuoteScreen_BuyButton");
    final By buyNowButton = By.xpath("//*[starts-with(@name, 'Buy Now:')]");
    final By buyNowButtonGetQuote = By.xpath("//XCUIElementTypeStaticText[@name=\"Buy Now: $10.35\"]");
    final By hoursWheelPicker = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel[1]");
    final By minutesWheelPicker = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel[2]");
    final By selectedTime = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[5]/XCUIElementTypeStaticText[2]");
    final By parkingFeeText = MobileBy.AccessibilityId("Location includes $0.35 Convenience Fee");
    final By selectedZone = By.xpath("//*[@name='Main Entrance']");
    final By vehicleName = By.xpath("//*[starts-with(@name, 'PLATE')]");
    final By periodOfTimeNotificationMessage = MobileBy.AccessibilityId("You must select a period of time");
    final By pleaseTryAgainButton = By.xpath("//XCUIElementTypeStaticText[@name=\"Please Try Again\"]");
    final By durationWheelsContainer = By.xpath("//*[@type='XCUIElementTypePicker']");
    final By navigateBackButton = MobileBy.AccessibilityId("Back");
    final By freeParkingMessage = By.xpath("//*[starts-with(@value, 'Parking is free for the time you requested.')]");

    final By confirmYourVehiclePopUp = By.xpath("//XCUIElementTypeStaticText[@name=\"Confirm your vehicle\"]");
    final By yesBuyParkingPopUpButton = MobileBy.AccessibilityId("Yes, Buy Parking");
    final By changeVehiclePopupButton = MobileBy.AccessibilityId("Change Vehicle");

    public BuyParkingPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isBuyParkingPageDisplayed() {
        log.info("Check is Buy Parking page displayed.");
        return waitForElementToBeDisplayed(buyParkingPageId);
    }

    public boolean areParkingDurationWheelsDisplayed() {
        log.info("Checking are parking duration wheels displayed.");
        return waitForElementToBeDisplayed(durationWheelsContainer);
    }

    public boolean isCorrectLotDisplayed(final String lotName) {
        String lotNameToSearch;
        if (lotName.equals("Macy's")) {
            lotNameToSearch = lotName+" ";
        } else {
            lotNameToSearch = lotName;
        }
        log.info("Checking is lot name displayed on Buy Parking screen.");
        final String actualLotName = driver.findElement(MobileBy.AccessibilityId(lotNameToSearch)).getText().trim();
        return actualLotName.equals(lotName);
    }

    public void clickOnRequestRateButton() {
        waitAndClick(requestRateButton);
        log.info("Tapped on Request Rate button.");
    }

    public void clickOnBuyNowButton() {
        isElementEnabled(buyNowButton);
        final String buyNowButtonText = driver.findElement(buyNowButton).getText();
        final String totalParkingAmount = buyNowButtonText.replaceAll("\\w+ \\w+: ", "");
        log.info("Amount which user needs to pay is: " + totalParkingAmount);
        setParkingInformation(totalParkingAmount);
        waitAndClick(requestRateButton);
        log.info("Tapped on Buy Now: {} button.", totalParkingAmount);
    }

    public void checkThatBuyEndTimeIsCorrect(final int hours, final int minutes) {
        if (driver.findElement(selectedTime).getText().contains("AM") ||
                driver.findElement(selectedTime).getText().contains("PM")) {
            final String format = "h':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(selectedTime).getText()
                            .contains(ParkingTimeUtil.buyParkingExpectedTime(
                                    driver.findElement(selectedTime).getText(), hours, minutes, DateTimeFormatter.ofPattern(format))));

            storage.getMobileEntity().setTimeFormat(format);
        } else {
            final String format = "k':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(selectedTime).getText()
                            .contains(ParkingTimeUtil.buyParkingExpectedTime(
                                    driver.findElement(selectedTime).getText(), hours, minutes, DateTimeFormatter.ofPattern(format))));

            storage.getMobileEntity().setTimeFormat(format);
        }
        storage.getLastParkingEntity().setSelectedHours(hours);
        storage.getLastParkingEntity().setSelectedMinutes(minutes);

        log.info("End time of parking duration is correct.");
    }

    public void enterHours(final int hours) {
        waitForElementToBeDisplayed(hoursWheelPicker);
        String hoursToSend;
        if (hours == 1) {
            hoursToSend = hours + " hour";
        } else {
            hoursToSend = hours + " hours";
        }
        waitAndSendKeys(hoursWheelPicker, hoursToSend);
        log.info("Selected {} for parking duration.", hoursToSend);
    }

    public void enterMinutes(final int minutes) {
        waitForElementToBeDisplayed(minutesWheelPicker);
        final String minutesToSend = minutes + " minutes";
        waitAndSendKeys(minutesWheelPicker, minutesToSend);
        log.info("Selected {} for parking duration.", minutesToSend);
    }

    public boolean isQuoteFeeCorrect() {
        waitForElementToBeDisplayed(parkingFeeText);
        log.info("Checking is Fee message shown.");
        log.info("Fee message is shown with text: " + driver.findElement(parkingFeeText).getText());
        return driver.findElement(parkingFeeText).getText().contains("Convenience Fee");
    }

    public boolean isConfirmYourVehiclePopUpDisplayed() {
        log.info("Check is confirm your vehicle pop up displayed.");
        return waitForElementToBeDisplayed(confirmYourVehiclePopUp);
    }

    public boolean isFreeParkingTextDisplayed() {
        log.info("Check is free parking message displayed.");
        return waitForElementToBeDisplayed(freeParkingMessage);
    }

    public void clickOnYesButtonParkingPopUpButton() {
        waitAndClick(yesBuyParkingPopUpButton);
        log.info("Clicked on Yes Buy parking button.");
    }

    public void clickChangeVehiclePopupButton() {
        log.info("Clicked change vehicle button on pop up.");
        waitAndClick(changeVehiclePopupButton);
    }

    public void selectVehicle(final String vehicle) {
        waitAndClick(vehicleName);
        log.info("Changing vehicle to {}", vehicle);
        waitAndSendKeys(By.xpath("//*[@type='XCUIElementTypePickerWheel']"), vehicle);
    }

    public boolean isPleaseTryAgainButtonEnabled() {
        log.info("Checking is Please Try Again button displayed.");
        return isElementEnabled(pleaseTryAgainButton);
    }

    public boolean isSnackBarMessageDisplayed () {
        log.info("Checking is notification message(You must select a period of time) displayed.");
        return waitForElementToBeDisplayed(periodOfTimeNotificationMessage);
    }

    public void isBuyNowButtonDisplayed() {
        log.info("Checking is Buy Now button displayed.");
        Awaitility.await().atMost(30, TimeUnit.SECONDS)
                .pollDelay(5, TimeUnit.SECONDS)
                .pollInterval(5, TimeUnit.SECONDS)
                .until(() -> (isElementEnabled(buyNowButtonGetQuote)));
    }

    public boolean isRequestRateButtonDisplayed() {
        log.info("Checking is Request Rate button displayed.");
        return waitForElementToBeDisplayed(requestRateButton);
    }

    public void clickOnNavigateBackButton() {
        waitAndClick(navigateBackButton);
    }

    private void setParkingInformation(final String totalParkingAmount) {
        final String parkingFee;
        if (!totalParkingAmount.equals("$0.00")) {
            parkingFee = driver.findElement(parkingFeeText).getText()
                    .replaceAll("[A-z]\\S*", "").trim();
        } else {
            parkingFee = "$0.00";
        }
        final String zone = driver.findElement(selectedZone).getText();
        final String vehicle = driver.findElement(vehicleName).getText();
        final String fullSelectedTime = driver.findElement(selectedTime).getText();
        storage.getLastParkingEntity().setVehicle(vehicle);
        storage.getLastParkingEntity().setParkingFee(parkingFee);
        storage.getLastParkingEntity().setTotalParkingAmount(totalParkingAmount);
        storage.getLastParkingEntity().setZone(zone);
        storage.getLastParkingEntity().setSelectedTime(fullSelectedTime);
        storage.getLastParkingEntity().setStartTime(ParkingTimeUtil.getBuyParkingInitialStartTime());
        storage.getLastParkingEntity().setEndTime(ParkingTimeUtil.getBuyParkingExpectedEndTime());
    }
}
