package us.reef.mobile.automation.ui.pages.account;

import io.appium.java_client.ios.IOSElement;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.rest.data.creditcard.CreditCard;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.*;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreditCardPage extends BasePage {

    @Autowired
    private Storage storage;

    final By creditCardPageId = MobileBy.AccessibilityId("Add your credit card");
    final By viewCreditCardsPageId = By.xpath("//*[@name='Credit Cards']");
    final By addCreditCardPageId = By.xpath("//*[@name='Add Card']");
    final By creditCardNumberField = By.xpath("//*[@value='Card number']");
    final By fullNameOnCardField = By.xpath("//*[@value='Full name on card']");
    final By fullNameOnCardFieldV14 = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeTextField[2]");
    final By cvvField = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeTextField[3]");
    final By monthSelection = By.xpath("//*[@value='Month']");
    final By monthSelectionV14 = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeOther[2]");
    final By yearSelection = By.xpath("//*[@value='Year']");
    final By yearSelectionV14 = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeOther[3]");
    final By addCreditCardButton = By.xpath("//*[@name='Add Credit Card']");
    final By creditCardSection = MobileBy.AccessibilityId("Pay Parking Notices");
    final By wheelPicker = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker");
    final By wheelPickerV13 = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker");
    final By addCreditCardButtonViewCreditCards = MobileBy.AccessibilityId("Add");
    final By creditCardsList = By.xpath("//*[@type='XCUIElementTypeTable']/XCUIElementTypeCell");

    final By doneButton = MobileBy.AccessibilityId("Done");
    final By skipThisStepButton = By.xpath("//*[@name='Skip this step']");

    public CreditCardPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isCreditCardPageDisplayed() {
        return waitForElementToBeDisplayed(creditCardPageId);
    }

    public boolean isViewCreditCardsPageDisplayed() {
        log.info("Checking is view credit cards page displayed.");
        return waitForElementToBeDisplayed(viewCreditCardsPageId);
    }

    public boolean isAddCreditCardPageDisplayed() {
        log.info("Checking is add credit card page displayed.");
        return waitForElementToBeDisplayed(addCreditCardPageId);
    }

    public boolean isCreditCardSectionDisplayed() {
        return waitForElementToBeDisplayed(creditCardSection);
    }

    public void clickOnSkipThisStepButton() {
        waitAndClick(skipThisStepButton);
    }

    public void clickOnAddCreditCardOnViewCreditCards() {
        waitAndClick(addCreditCardButtonViewCreditCards);
    }

    public List<IOSElement> getListOfCreditCards() {
        waitForElementToBeDisplayed(creditCardsList);
        return driver.findElements(creditCardsList);
    }

    public boolean isCreditCardPresentInTheList(final String creditCard) {
        return waitForElementToBeDisplayed(By.xpath("//*[@value='"+creditCard+"']"));
    }

    public boolean isFirstCreditCardPrimary() {
        return getListOfCreditCards().stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First credit card not found!"))
                .findElement(By.xpath("//*[@value='Primary']")).isDisplayed();
    }
    public void addTestCreditCard(final String creditCard) {
        final CreditCard creditCardNumber;
        final String cardName = "Automated Test";
        final String cvv = "357";
        switch (creditCard.toUpperCase()) {
            case "VISA":
                creditCardNumber = CreditCard.VISA;
                break;
            case "MASTER CARD":
                creditCardNumber = CreditCard.MASTER_CARD;
                break;
            case "AMERICAN EXPRESS":
                creditCardNumber = CreditCard.AMERICAN_EXPRESS;
                break;
            case "DISCOVER":
                creditCardNumber = CreditCard.DISCOVER;
                break;
            default:
                throw new MobileAutomationException("Credit card {} is not supported!", creditCard);
        }
        waitAndSendKeys(creditCardNumberField, creditCardNumber.card);
        log.info("Entered credit card number {}.", creditCardNumber.card);
        tapOnDoneButtonIfItIsDisplayed();
        if (getOsVersion().contains("14")) {
            waitAndSendKeys(fullNameOnCardFieldV14, cardName);
        } else {
            waitAndSendKeys(fullNameOnCardField, cardName);
        }
        log.info("Entered Card name {}.", cardName);
        tapOnDoneButtonIfItIsDisplayed();
        waitAndSendKeys(cvvField, cvv);
        tapOnDoneButtonIfItIsDisplayed();
        log.info("Entered CVV number {}.", cvv);
        if (getOsVersion().contains("14")) {
            waitAndClick(monthSelectionV14);
        } else {
            waitAndClick(monthSelection);
        }
        if (getOsVersion().contains("13") || getOsVersion().contains("14")) {
            longPressAndMoveTo(getDriver(), wheelPickerV13, doneButton);
        } else {
            longPressAndMoveTo(getDriver(), wheelPicker, doneButton);
        }
        log.info("Selected month.");
        tapOnDoneButtonIfItIsDisplayed();
        if (getOsVersion().contains("14")) {
            waitAndClick(yearSelectionV14);
        } else {
            waitAndClick(yearSelection);
        }
        if (getOsVersion().contains("13") || getOsVersion().contains("14")) {
            longPressAndMoveTo(getDriver(), wheelPickerV13, doneButton);
        } else {
            longPressAndMoveTo(getDriver(), wheelPicker, doneButton);
        }
        log.info("Selected year.");
        tapOnDoneButtonIfItIsDisplayed();
        waitAndClick(addCreditCardButton);
        log.info("Tapped on add credit card button.");
        final List<String> creditCardList = new ArrayList<>();
        creditCardList.add(creditCard);
        storage.getLastUserEntity().setCreditCards(creditCardList);
    }

    private void tapOnDoneButtonIfItIsDisplayed() {
        if(isElementPresentAndDisplayed(doneButton)) {
            waitAndClick(doneButton);
        }
    }
}