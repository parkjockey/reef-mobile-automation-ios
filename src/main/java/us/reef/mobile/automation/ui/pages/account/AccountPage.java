package us.reef.mobile.automation.ui.pages.account;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class AccountPage extends BasePage {

    final By accountPageId = By.xpath("//*[@name='Account']");
    final By vehicles = By.xpath("//*[@name='Vehicles']");
    final By creditCards = By.xpath("//*[@name='Credit Cards']");
    final By changePassword = By.xpath("//*[@name='Change Password']");
    final By backButton = MobileBy.AccessibilityId("Back");

    public AccountPage(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isAccountPageDisplayed() {
        log.info("Checking is account page displayed.");
        return waitForElementToBeDisplayed(accountPageId);
    }

    public void clickOnVehicles() {
        waitAndClick(vehicles);
        log.info("Tapped on Vehicles menu item.");
    }

    public void clickOnCreditCards() {
        waitAndClick(creditCards);
        log.info("Tapped on Credit Cards menu item.");
    }

    public void clickOnBackButton() {
        waitAndClick(backButton);
    }

    public void clickOnChangePassword() {
        waitAndClick(changePassword);
    }
}
