package us.reef.mobile.automation.ui.pages.parking;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class SearchLotPage extends BasePage {

    @Autowired
    private Storage storage;

    final By searchForField = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField");
    final By searchResults = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTable/XCUIElementTypeCell");
    final By firstLotFromResult = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTable/XCUIElementTypeCell[1]");
    final By payNowButton = MobileBy.AccessibilityId("Pay Now");

    public SearchLotPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public void enterLotNameInTypeNameSearchBox(final String lotName) {
        waitAndSendKeys(searchForField, lotName);
        log.info("Entered partial name of lot name starting with {}", lotName);
    }

    public void searchForLot(final String lot) {
        final ParkingEntity parkingEntity = ParkingEntity.builder()
                .lotName(lot)
                .build();

        storage.getParkingEntities().add(parkingEntity);
        waitAndClick(payNowButton);
        waitAndClick(searchForField);
        waitAndSendKeys(searchForField, lot.toUpperCase());
        log.info("Searching for lot {}.", lot);
        waitAndClick(MobileBy.AccessibilityId(lot));
        log.info("Tapped on lot {}.", lot);
    }

    public boolean isSearchFieldDisplayed() {
        log.info("Checking that Pay Now Search box is displayed.");
        return waitIfElementIsGoingToBeVisibleAfterSomeTime(searchForField, TimeUnit.SECONDS, 20);
    }

    public boolean doResultsContainsCorrectPartialLotName(final String lotName) {
        List<Boolean> listOfResults = new ArrayList<>();
        driver.findElements(searchResults).forEach(iosElement ->
                listOfResults.add(iosElement.findElement(By.xpath("XCUIElementTypeStaticText[1]")).getText().contains(lotName)));
        return !listOfResults.contains(false);
    }

    public String getFirstLotNameFromResultList() {
        waitForElementToBeDisplayed(searchResults);
        return driver.findElement(firstLotFromResult).findElement(By.xpath("XCUIElementTypeStaticText[1]")).getText();
    }

    public void clickOnFirstLotFromList() {
        waitForElementToBeDisplayed(firstLotFromResult);
        waitAndClick(firstLotFromResult);
        log.info("Tapped on lot name from result list.");
    }

    public void clickOnPayNowButton() {
        waitAndClick(payNowButton);
    }

    public boolean isNoResultsFound(final String lotName) {
        log.info("Checking that not results are returned.");
        return waitForElementToBeDisplayed(By.xpath("//*[@value='There are no results for "+lotName+"']"));
    }
}