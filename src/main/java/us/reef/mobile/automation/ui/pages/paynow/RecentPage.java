package us.reef.mobile.automation.ui.pages.paynow;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class RecentPage extends BasePage {

    @Autowired
    private Storage storage;

    final By recentPageId = By.xpath("//*[(@value='Select from your most recent lots below') or (@value='After ordering, your most recent lots will show here')]");
    final By recentLotsList = By.xpath("//*[@type='XCUIElementTypeTable']/XCUIElementTypeCell");

    public RecentPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isRecentPageDisplayed() {
        log.info("Checking is Recent Lots screen displayed.");
        return waitForElementToBeDisplayed(recentPageId);
    }

    public int getNumberOfRecentLots() {
        return driver.findElements(recentLotsList).size();
    }

    public boolean isRecentLotWithNameDisplayed(final String lotName) {
        return waitForElementToBeDisplayed(By.xpath("//*[@name='"+lotName+"']"));
    }

    public boolean areLastParkedDatesCorrect(final String expectedFormat, final ParkingEntity parking) {
        final String expectedDate = ParkingTimeUtil.getLastParkedDate(expectedFormat);
        final String initialText = "//*[contains(@value, 'Last parked ";
        String expectedStartTimeFormatted1 = LocalTime.of(
                ParkingTimeUtil.extractHours(parking.getStartTime()),
                ParkingTimeUtil.extractMinutes(parking.getStartTime())
        ).format(DateTimeFormatter.ofPattern("h':'mm"));
        String expectedStartTimeFormatted2 = LocalTime.of(
                ParkingTimeUtil.extractHours(parking.getStartTime()),
                ParkingTimeUtil.extractMinutes(parking.getStartTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        String expectedStartTimeFormatted3 = LocalTime.of(
                ParkingTimeUtil.extractHours(parking.getStartTime()),
                ParkingTimeUtil.extractMinutes(parking.getStartTime())
        ).format(DateTimeFormatter.ofPattern("k':'mm"));
        String expectedStartTimeFormatted4 = LocalTime.of(
                ParkingTimeUtil.extractHours(parking.getStartTime()),
                ParkingTimeUtil.extractMinutes(parking.getStartTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("k':'mm"));
        if (expectedStartTimeFormatted1.substring(0, 2).contains(":")) {
            expectedStartTimeFormatted1 = "0" + expectedStartTimeFormatted1;
        }
        if (expectedStartTimeFormatted2.substring(0, 2).contains(":")) {
            expectedStartTimeFormatted2 = "0" + expectedStartTimeFormatted2;
        }
        if (expectedStartTimeFormatted3.substring(0, 2).contains(":")) {
            expectedStartTimeFormatted3 = "0" + expectedStartTimeFormatted3;
        }
        if (expectedStartTimeFormatted4.substring(0, 2).contains(":")) {
            expectedStartTimeFormatted4 = "0" + expectedStartTimeFormatted4;
        }
        return isElementPresentAndDisplayed(By.xpath(initialText + expectedDate + " - " + expectedStartTimeFormatted1 + "')]")) ||
                isElementPresentAndDisplayed(By.xpath(initialText + expectedDate + " - " + expectedStartTimeFormatted2 + "')]")) ||
                isElementPresentAndDisplayed(By.xpath(initialText + expectedDate + " - " + expectedStartTimeFormatted3 + "')]")) ||
                isElementPresentAndDisplayed(By.xpath(initialText + expectedDate + " - " + expectedStartTimeFormatted4 + "')]"));
    }

    public String getNameOfFirstLotFromTheList() {
        return driver.findElements(recentLotsList).stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First lot from the recent lots is not found!"))
                .findElements(By.xpath("//*[@type='XCUIElementTypeStaticText']"))
                .stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("Cannot find first element in recent lots!!"))
                .getText();
    }

    public void tapOnFirstLotFromTheList() {
        driver.findElements(recentLotsList).stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First lot from the recent lots is not found!"))
                .click();
    }
}