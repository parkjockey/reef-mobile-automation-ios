package us.reef.mobile.automation.ui.pages.parking;

import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ParkingSessionPage extends BasePage {

    final By extendParkingButton = MobileBy.AccessibilityId("Extend Parking");
    final By startTime = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
    final By endTime = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]");
    final By purchaseTime = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeStaticText[2]");
    final By parkingLot = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[4]/XCUIElementTypeStaticText[2]");
    final By zone = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[5]/XCUIElementTypeStaticText[2]");
    final By vehicle = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[6]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]");
    final By purchaseNumber = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[7]/XCUIElementTypeStaticText[2]");
    final By totalAmount = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[8]/XCUIElementTypeStaticText[2]");
    final By creditCard = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[9]/XCUIElementTypeStaticText[2]");
    final By mapButton = MobileBy.AccessibilityId("Map");

    public ParkingSessionPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isParkingSessionPageDisplayed() {
        log.info("Checking is Parking Session page displayed, session timer must be present.");
        return waitForElementToBeDisplayed(extendParkingButton);
    }

    public boolean isExtendParkingButtonDisplayed() {
        return waitForElementToBeDisplayed(extendParkingButton);
    }

    public boolean isParkingLotNameDisplayed(final String lotName) {
        log.info("Checking is parking lot name displayed and correct.");
        return driver.findElement(MobileBy.AccessibilityId(lotName)).isDisplayed();
    }

    public boolean isTotalAmountDisplayed(final String totalAmount) {
        log.info("Checking is total amount displayed and correct.");
        return driver.findElement(MobileBy.AccessibilityId(totalAmount)).isDisplayed();
    }

    public boolean isVehiclePlateDisplayed(final String vehiclePlate) {
        log.info("Checking is vehicle plate displayed and correct.");
        return driver.findElement(MobileBy.AccessibilityId(vehiclePlate)).isDisplayed();
    }

    public void tapExtendParkingButton() {
        log.info("Taps Extend parking button.");
        waitAndClick(extendParkingButton);
    }

    public void tapMapButton() {
        log.info("Taps Map Button.");
        waitAndClick(mapButton);
    }

    public String getStartTIme() {
        waitForElementToBeDisplayed(startTime);
        return driver.findElement(startTime).getText();
    }

    public String getEndTime() {
        waitForElementToBeDisplayed(endTime);
        return driver.findElement(endTime).getText();
    }

    public String getPurchaseTime() {
        waitForElementToBeDisplayed(purchaseTime);
        return driver.findElement(purchaseTime).getText();
    }

    public String getParkingLot() {
        waitForElementToBeDisplayed(parkingLot);
        return driver.findElement(parkingLot).getText();
    }

    public String getZone() {
        waitForElementToBeDisplayed(zone);
        return driver.findElement(zone).getText();
    }

    public String getVehicle() {
        waitForElementToBeDisplayed(vehicle);
        return driver.findElement(vehicle).getText();
    }

    public String getPurchaseNumber() {
        waitForElementToBeDisplayed(purchaseNumber);
        return driver.findElement(purchaseNumber).getText();
    }

    public String getTotalAmount() {
        waitForElementToBeDisplayed(totalAmount);
        return driver.findElement(totalAmount).getText();
    }

    public String getCreditCard() {
        waitForElementToBeDisplayed(creditCard);
        return driver.findElement(creditCard).getText();
    }
}
