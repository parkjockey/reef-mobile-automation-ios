package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VerifyPhonePage extends BasePage {

    final By verificationPageId = MobileBy.AccessibilityId("Verify your mobile phone");
    final By verificationPageIdSE = MobileBy.AccessibilityId("A verification code was sent to your mobile phone");
    final By verificationCodeFiled = MobileBy.AccessibilityId("VerifyPhonePage_verifyPhoneTextField");
    final By verificationCodeFieldSE = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile - UAT\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeTextField");
    final By verifyPhoneButton = MobileBy.AccessibilityId("Verify Phone");

    public VerifyPhonePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isVerificationPageLoaded() {
        if (getMobilePhone().equals("iPhone SE")) {
            return waitForElementToBeDisplayed(verificationPageIdSE);
        } else {
            return waitForElementToBeDisplayed(verificationPageId);
        }
    }

    public void enterVerificationCode(final String verificationCode) {
        if (getMobilePhone().equals("iPhone SE")) {
            waitAndSendKeys(verificationCodeFieldSE, verificationCode);
        } else {
            waitAndSendKeys(verificationCodeFiled, verificationCode);
        }
        log.info("Entered verification code: " + verificationCode);
        if (waitIfElementIsGoingToBeVisibleAfterSomeTime(verifyPhoneButton, TimeUnit.SECONDS, 10)) {
            waitAndClick(verifyPhoneButton);
            log.info("User tapped on verify button.");
        }
    }
}