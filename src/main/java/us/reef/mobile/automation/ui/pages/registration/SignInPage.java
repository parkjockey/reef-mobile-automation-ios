package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class SignInPage extends BasePage {

    final By signInPageId = MobileBy.AccessibilityId("Log in to your account");
    final By emailField = MobileBy.AccessibilityId("LoginPage_emailTextField");
    final By passwordField = MobileBy.AccessibilityId("LoginPage_passwordTextField");
    final By signInButton = MobileBy.AccessibilityId("Log In");

    public SignInPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isSignInPageDisplayed() {
        log.info("Check is Sing Up page displayed.");
        return waitForElementToBeDisplayed(signInPageId);
    }

    public void enterEmailAndPassword(final String email, final String password) {
        waitAndClick(emailField);
        waitAndSendKeys(emailField, email);
        waitAndClick(passwordField);
        waitAndSendKeys(passwordField, password);
        log.info("User entered email: {} and password: {}.", email, password);
    }

    public void clickSingInButton() {
        waitAndClick(signInPageId);
        waitAndClick(signInButton);
        log.info("User clicked sign in button.");
    }
}