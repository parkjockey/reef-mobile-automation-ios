package us.reef.mobile.automation.storage.user;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder(toBuilder = true)
public class UserEntity {
    private String email;
    private String password;
    private String mobilePhoneNumber;
    private String verificationCode;
    private List<String> licensePlates;
    private List<String> states;
    private List<String> creditCards;
    private String token;
}
