package us.reef.mobile.automation.storage.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MobileEntity {

    private String timeFormat;
}
