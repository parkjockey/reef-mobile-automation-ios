package us.reef.mobile.automation.storage;

import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.scenario.ScenarioEntity;
import us.reef.mobile.automation.storage.testrail.TestRailEntity;
import us.reef.mobile.automation.storage.user.MobileEntity;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.storage.user.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Component
@Scope("cucumber-glue")
public class Storage {

    private ScenarioEntity scenarioEntity = new ScenarioEntity();
    private TestRailEntity testRailEntity = new TestRailEntity();
    private MobileEntity mobileEntity = new MobileEntity();
    private List<UserEntity> userEntities = new ArrayList<>();
    private List<ParkingEntity> parkingEntities = new ArrayList<>();

    /**
     * Get last User from storage.
     *
     * @return {@link UserEntity}
     */
    public UserEntity getLastUserEntity() {
        return this.getUserEntities().get(getUserEntities().size() - 1);
    }

    /**
     * Get last Parking from storage.
     *
     * @return {@link ParkingEntity}
     */
    public ParkingEntity getLastParkingEntity() {
        return this.getParkingEntities().get(getParkingEntities().size() - 1);
    }

    /**
     * Get First Parking from storage.
     *
     * @return {@link ParkingEntity}
     */
    public ParkingEntity getFirstParking() {
        return this.getParkingEntities().stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("There is not first parking entity, check storage!"));
    }
}
