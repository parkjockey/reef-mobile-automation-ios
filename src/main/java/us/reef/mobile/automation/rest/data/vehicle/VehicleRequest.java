package us.reef.mobile.automation.rest.data.vehicle;

import us.reef.mobile.automation.rest.data.AppVersion;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehicleRequest {
    private int action;
    private String licensePlate;
    private String stateProvince;
    private AppVersion appVersion;
}
