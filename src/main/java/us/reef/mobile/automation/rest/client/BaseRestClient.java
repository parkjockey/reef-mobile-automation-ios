package us.reef.mobile.automation.rest.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Map;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.DecoderConfig.decoderConfig;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.config.LogConfig.logConfig;
import static io.restassured.config.RedirectConfig.redirectConfig;
import static org.hamcrest.core.IsNot.not;

@Slf4j
public class BaseRestClient {

    // HTTP Timeouts in miliseconds
    private static final int HTTP_CONNECTION_TIMEOUT = 180000; // the time to establish the connection with the remote host
    private static final int HTTP_SOCKET_TIMEOUT = 600000; // the time waiting for data – after the connection was established; maximum time of inactivity between two data packets
    private static final int HTTP_CONNECTION_MANAGER_TIMEOUT = 10000; // the time to wait for a connection from the connection manager/pool

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd[HH:mm:ss]";

    @Getter(AccessLevel.PRIVATE)
    private final RequestSpecBuilder defaultRequestSpecBuilder = new RequestSpecBuilder();
    @Getter(AccessLevel.PRIVATE)
    final ObjectMapperConfig objectMapperConfig = new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) -> {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.registerModule(new SimpleModule().addSerializer(BigInteger.class, new ToStringSerializer()));
        objectMapper.registerModule(new JodaModule());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(new SimpleDateFormat(DATE_TIME_FORMAT));
        return objectMapper;
    });

    public BaseRestClient(final String baseUrl) {
        setBaseUri(baseUrl);
        configSetup();
        setCommonHeaders();
    }

    public BaseRestClient(final String baseUrl, final String token) {
        setBaseUri(baseUrl);
        configSetup();
        setCommonHeaders();
        getDefaultRequestSpecBuilder().addHeader("Authorization", token);
    }

    private void setBaseUri(final String baseUrl) {
        getDefaultRequestSpecBuilder().setBaseUri(baseUrl);
    }

    private void configSetup() {
        getDefaultRequestSpecBuilder().setConfig(
                config.logConfig(logConfig().enablePrettyPrinting(true))
                        .encoderConfig(encoderConfig().defaultContentCharset("UTF-8"))
                        .decoderConfig(decoderConfig().defaultContentCharset("UTF-8"))
                        .objectMapperConfig(getObjectMapperConfig())
                        .redirect(redirectConfig().followRedirects(false))
                        .httpClient(HttpClientConfig.httpClientConfig()
                                .setParam("http.connection.timeout", HTTP_CONNECTION_TIMEOUT)
                                .setParam("http.socket.timeout", HTTP_SOCKET_TIMEOUT)
                                .setParam("http.connection-manager.timeout", HTTP_CONNECTION_MANAGER_TIMEOUT)));
    }

    private void setCommonHeaders() {
        getDefaultRequestSpecBuilder()
                .setContentType(ContentType.JSON);
        getDefaultRequestSpecBuilder()
                .setAccept(ContentType.JSON)
                .setAccept(ContentType.TEXT)
                .setAccept(ContentType.ANY);
    }

    public Response post(final Object body, final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        addBody(body, requestSpecificationBuilder);

        return given()
                .spec(requestSpecificationBuilder.build())
                .when()
                .post(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();
    }

    public Response get(final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        return given()
                .spec(requestSpecificationBuilder.build())
                .when()
                .get(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();
    }

    public Response get(final Map<String, String> queryParams, final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        return given()
                .spec(requestSpecificationBuilder.build())
                .queryParams(queryParams)
                .when()
                .get(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();
    }

    public String getToken(final Object body, final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        addBody(body, requestSpecificationBuilder);

        final Response response = given()
                .spec(requestSpecificationBuilder.build())
                .when()
                .put(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();

        if (response.getStatusCode() != HttpStatus.SC_OK || StringUtils.isEmpty(response.jsonPath().getString("Token"))) {
            throw new MobileAutomationException("Token not fetched! Expected status {} but was {}!", HttpStatus.SC_OK, response.getStatusCode());
        }

        return "Bearer " + response.jsonPath().getString("Token");
    }

    public Response delete(final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        return given()
                .spec(requestSpecificationBuilder.build())
                .when()
                .delete(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();
    }

    public Response put(final Object body, final String path) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        addBody(body, requestSpecificationBuilder);

        return given()
                .spec(requestSpecificationBuilder.build())
                .when()
                .put(path)
                .then()
                .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                .response();
    }

    public Response putStatusOnBrowserStatus(final Object body,
                                             final String path,
                                             final String username,
                                             final String password) {
        final RequestSpecBuilder requestSpecificationBuilder = new RequestSpecBuilder()
                .addRequestSpecification(getDefaultRequestSpecBuilder().build());

        addBody(body, requestSpecificationBuilder);

        return given()
                .auth()
                .preemptive()
                    .basic(username, password)
                    .spec(requestSpecificationBuilder.build())
                .when()
                    .put(path)
                .then()
                    .statusCode(not(HttpStatus.SC_UNAUTHORIZED))
                .extract()
                    .response();
    }

    /**
     * Sets Body to Request Specification.
     *
     * @param body                        payload body
     * @param requestSpecificationBuilder request specification builder
     */
    private void addBody(final Object body, final RequestSpecBuilder requestSpecificationBuilder) {
        if (null != body) {
            requestSpecificationBuilder.setBody(body);
        }
    }
}
