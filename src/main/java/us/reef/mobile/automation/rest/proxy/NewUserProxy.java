package us.reef.mobile.automation.rest.proxy;

import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.rest.client.BaseRestClient;
import us.reef.mobile.automation.rest.data.AppVersion;
import us.reef.mobile.automation.rest.data.UserDSO;
import us.reef.mobile.automation.rest.data.verification.VerificationRequest;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.UserEntity;
import us.reef.mobile.automation.utils.RegisterNewUserUtil;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class NewUserProxy {

    @Autowired
    private Storage storage;

    @Value("${reef-mobile.url}")
    private String baseUrl;

    public void createNewUser() {
        final BaseRestClient baseRestClient = new BaseRestClient(baseUrl);

        final String email = RegisterNewUserUtil.getRandomEmail();
        final String password = RegisterNewUserUtil.getRandomPassword(12);
        final String mobilePhoneNumber = RegisterNewUserUtil.generateRandomMobilePhoneNumber();

        final UserDSO userDSO = UserDSO.builder()
                .emailAddress(email)
                .mobilePhoneNumber(mobilePhoneNumber)
                .password(password)
                .appVersion(new AppVersion())
                .build();

        final Response response = baseRestClient.post(userDSO, "hangTagAccounts/Register");

        if (response.getStatusCode() != HttpStatus.SC_OK) {
            throw new MobileAutomationException("User not created! Expected status {} but was {}, Body: {}", HttpStatus.SC_OK, response.getStatusCode(), response.getBody().prettyPrint());
        }

        log.info("Created new user with email: {}, password: {}, mobile phone: {}", email, password, mobilePhoneNumber);

        final UserEntity user = UserEntity.builder()
                .email(email)
                .password(password)
                .mobilePhoneNumber(mobilePhoneNumber)
                .build();
        storage.getUserEntities().add(user);
    }

    public void getVerificationCode() {
        final BaseRestClient baseRestClient = new BaseRestClient(baseUrl);

        final UserEntity userEntity = storage.getLastUserEntity();

        final Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("emailAddress", userEntity.getEmail());
        queryParameters.put("code", "HangTagCanada");

        final Response response = baseRestClient.get(queryParameters, "hangTagAccounts/VerificationCode");

        if (response.getStatusCode() != HttpStatus.SC_OK) {
            throw new MobileAutomationException("Verification not fetched! Expected status {} but was {}!", HttpStatus.SC_OK, response.getStatusCode());
        }
        final String verificationCode = response.getBody().asString().replaceAll("\"","");
        log.info("Verification Code is: {}", verificationCode);
        storage.getLastUserEntity().setVerificationCode(verificationCode);
    }

    /**
     * Get user token and save it in user entity for later on requests
     */
    public void getToken() {
        getVerificationCode();

        final BaseRestClient baseRestClient = new BaseRestClient(baseUrl);

        final UserEntity userEntity = storage.getLastUserEntity();

        final VerificationRequest verificationRequest = VerificationRequest.builder()
                .emailAddress(userEntity.getEmail())
                .password(userEntity.getPassword())
                .verificationCode(userEntity.getVerificationCode())
                .appVersion(new AppVersion())
                .build();

        storage.getLastUserEntity().setToken(baseRestClient.getToken(verificationRequest,"hangTagAccounts/VerifyPhone"));
    }
}
