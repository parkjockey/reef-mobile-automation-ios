package us.reef.mobile.automation.rest.data;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDSO {
    @Builder.Default
    private String countryCode = "USA";
    private String emailAddress;
    @Builder.Default
    private String mobilePhoneCountryCode = "1";
    private String mobilePhoneNumber;
    private String password;
    private AppVersion appVersion;
}
