package us.reef.mobile.automation.stepdefs.rest;

import us.reef.mobile.automation.rest.proxy.VehicleProxy;
import groovy.util.logging.Slf4j;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class VehicleStepDef {

    @Autowired
    private VehicleProxy vehicleProxy;

    @When("^Added license plate (.*) with state (DE)$")
    public void addedLicensePlate(final String licensePlate, final String state) {
        vehicleProxy.addVehicle(licensePlate, state);
    }
}
