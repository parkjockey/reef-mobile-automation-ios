package us.reef.mobile.automation.stepdefs.ui.user;

import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.UserEntity;
import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.parking.SearchLotPage;
import us.reef.mobile.automation.ui.pages.popup.PopUpPage;
import us.reef.mobile.automation.ui.pages.registration.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private ReefInitialPage reefInitialPage;

    @Autowired
    private SignInPage signInPage;

    @Autowired
    private VerifyPhonePage verifyPhonePage;

    @Autowired
    private PopUpPage popUpPage;

    @Autowired
    private HomePage homePage;

    @Autowired
    private SearchLotPage searchLotPage;

    @Given("^User tapped on sign up button$")
    public void tappedSignUpButton() {
        popUpPage.waitForNotificationsPopUpToBeDisplayedAndAllowIt();
        assertThat(reefInitialPage.isReefInitialPageLoaded()).as("Reef initial page is not loaded!").isTrue();
        reefInitialPage.clickSignUpButton();
    }

    @Given("^User tap(?:s|ped)? on login button$")
    public void userTappedOnLoginButton() {
        popUpPage.waitForNotificationsPopUpToBeDisplayedAndAllowIt();
        assertThat(reefInitialPage.isReefInitialPageLoaded()).as("Reef initial page is not loaded!").isTrue();
        reefInitialPage.clickLogInButton();
    }

    @When("^(?:Signs|Signed)? in with valid credentials$")
    public void signsInWithValidCredentials() {
        assertThat(signInPage.isSignInPageDisplayed()).as("Sign in page is no loaded!").isTrue();

        final UserEntity user = storage.getLastUserEntity();
        signInPage.enterEmailAndPassword(user.getEmail(), user.getPassword());
        signInPage.clickSingInButton();
    }

    @When("^Enter(?:s|ed)? verification code$")
    public void entersVerificationCode() {
        final UserEntity user = storage.getLastUserEntity();
        verifyPhonePage.enterVerificationCode(user.getVerificationCode());
    }

    @Then("^(?:User )?(?:[Ww]ill be|is)? logged in successfully$")
    public void loggedInSuccessfully() {
        popUpPage.allowLocationAccess();
        assertThat(homePage.isHomePageDisplayed()).as("Home page is not displayed!").isTrue();
        assertThat(searchLotPage.isSearchFieldDisplayed()).as("Pay Now Search Box is not displayed!").isTrue();
        assertThat(homePage.isMapDisplayed()).as("Map Section is not displayed!").isTrue();
    }
}