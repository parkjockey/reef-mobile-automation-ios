package us.reef.mobile.automation.stepdefs.ui.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.account.AccountPage;

public class AccountStepDef {

    @Autowired
    private AccountPage accountPage;

    @Given("^Tap(?:s|ped)? on [Vv]ehicles menu item$")
    public void tapOnVehiclesMenuItem() {
        accountPage.clickOnVehicles();
    }

    @Given("^Tap(?:s|ped)? on [Cc]redit [Cc]ards menu item$")
    public void tapOnCreditCardsMenuItem() {
        accountPage.clickOnCreditCards();
    }

    @Given("^Navigates from Account screen to Home screen$")
    public void tapOnBackButton() {
        accountPage.clickOnBackButton();
    }

    @When("^Tap(?:s|ped)? on Change Password menu item$")
    public void tapOnChangePassword() {
        accountPage.clickOnChangePassword();
    }
}