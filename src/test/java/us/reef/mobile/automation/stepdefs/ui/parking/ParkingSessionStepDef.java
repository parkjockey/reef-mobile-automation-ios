package us.reef.mobile.automation.stepdefs.ui.parking;

import org.assertj.core.api.SoftAssertions;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.pages.parking.ParkingSessionPage;
import io.cucumber.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.popup.PopUpPage;
import us.reef.mobile.automation.utils.CreditCardUtil;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;


public class ParkingSessionStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private ParkingSessionPage parkingSessionPage;

    @Autowired
    private PopUpPage popUpPage;

    final Logger logger = LoggerFactory.getLogger(ParkingSessionStepDef.class);

    @Then("^Session information after buying will be correct$")
    public void parkingSessionStarts() {
        assertThat(parkingSessionPage.isExtendParkingButtonDisplayed())
                .as("Extend parking button is not displayed.").isTrue();
        logger.info("User is navigated to parking session.");
        final String lotName = storage.getLastParkingEntity().getLotName();
        final String totalAmount = storage.getLastParkingEntity().getTotalParkingAmount();
        final String vehiclePlate = storage.getLastParkingEntity().getVehicle();
        assertThat(parkingSessionPage.isParkingLotNameDisplayed(lotName))
                .as("Lot name {} is not found.", lotName).isTrue();
        assertThat(parkingSessionPage.isTotalAmountDisplayed(totalAmount))
                .as("Total amount {} is not found.", totalAmount).isTrue();
        assertThat(parkingSessionPage.isVehiclePlateDisplayed(vehiclePlate))
                .as("Vehicle plate {} is not found.", vehiclePlate).isTrue();

        storage.getFirstParking().setPurchaseNumber(parkingSessionPage.getPurchaseNumber());
        logger.info("Lot name, Total amount and Vehicle plate are correct.");
    }

    @Then("^(?:Will )?[Tt]ap(?:s|ped)? extend parking button$")
    public void tapExtendParkingButton() {
        parkingSessionPage.tapExtendParkingButton();
    }

    @Then("^Parking session (?:is |will be )?(?:started|displayed)?$")
    public void parkingSessionIsDisplayed(){
        if (popUpPage.isRateAppNotificationDisplayed()) {
            popUpPage.tapRateAppNotNowButton();
        }
        assertThat(parkingSessionPage.isParkingSessionPageDisplayed()).as("Parking session page is not displayed!").isTrue();
        assertThat(parkingSessionPage.isExtendParkingButtonDisplayed()).as("Extend parking button is not displayed!").isTrue();
    }

    @Then("^Session information after extending will be correct$")
    public void sessionInformationWillBeCorrectAfterExtending() {
        final String actualParkingStartTime = parkingSessionPage.getStartTIme();
        final String actualParkingEndTime = parkingSessionPage.getEndTime();
        final String actualTotalTime = parkingSessionPage.getPurchaseTime();
        final String actualLotName = parkingSessionPage.getParkingLot();
        final String actualZone = parkingSessionPage.getZone();
        final String actualVehicle = parkingSessionPage.getVehicle();
        final String actualPurchaseNumber = parkingSessionPage.getPurchaseNumber();
        final String actualTotalAmount = parkingSessionPage.getTotalAmount();
        final String actualCreditCard = parkingSessionPage.getCreditCard();

        final ParkingEntity firstParking = storage.getFirstParking();
        logger.info("First parking entity: " + firstParking);
        logger.info("Start time of first parking: " + firstParking.getStartTime());
        final ParkingEntity secondParking = storage.getLastParkingEntity();
        final String expectedParkingStartTimeFirst = firstParking.getStartTime();
        final String expectedParkingStartTimeSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(firstParking.getStartTime()),
                ParkingTimeUtil.extractMinutes(firstParking.getStartTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedParkingEndTimeFirst = LocalTime.of(
                ParkingTimeUtil.extractHours(secondParking.getEndTime()),
                ParkingTimeUtil.extractMinutes(secondParking.getEndTime())
        ).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedParkingEndTimeSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(secondParking.getEndTime()),
                ParkingTimeUtil.extractMinutes(secondParking.getEndTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedLotName = secondParking.getLotName();
        final String expectedZone = firstParking.getZone();
        final String expectedVehiclePlate = secondParking.getVehicle();
        final String expectedPurchaseNumber = firstParking.getPurchaseNumber();
        final int selectedHours = firstParking.getSelectedHours() + secondParking.getSelectedHours();
        final int selectedMinutes = firstParking.getSelectedMinutes() + secondParking.getSelectedMinutes();
        final String expectedTotalTime = selectedHours + "h : " + selectedMinutes + "m";
        final String expectedCreditCard = CreditCardUtil.getCreditCardValueForParkingSessionPage(
                storage.getLastUserEntity().getCreditCards().get(0));
        logger.info("First parking total price: {}",firstParking.getTotalParkingAmount() );
        logger.info("Second parking total price: {}", secondParking.getTotalParkingAmount());
        final double firstTotalAmount = Double.parseDouble(firstParking.getTotalParkingAmount().replaceAll("\\$", ""));
        final double secondTotalAmount;
        if (secondParking.getTotalParkingAmount().contains("Extend for free")) {
            secondTotalAmount = 0;
        } else {
            secondTotalAmount = Double.parseDouble(secondParking.getTotalParkingAmount().replaceAll("\\$", ""));
        }
        final String expectedTotalAmount = "$"+(firstTotalAmount + secondTotalAmount);

        SoftAssertions.assertSoftly(softly -> {
            logger.info("Checking parking start time.");
            if (actualParkingStartTime.contains(expectedParkingStartTimeFirst)) {
                softly.assertThat(actualParkingStartTime).as("Start time is not correct!").contains(expectedParkingStartTimeFirst);
            } else {
                softly.assertThat(actualParkingStartTime).as("Start time is not correct").contains(expectedParkingStartTimeSecond);
            }
            logger.info("Checking parking end time.");
            if (actualParkingEndTime.contains(expectedParkingEndTimeFirst)) {
                softly.assertThat(actualParkingEndTime).as("End time is not correct!").contains(expectedParkingEndTimeFirst);
            } else {
                softly.assertThat(actualParkingEndTime).as("End time is not correct!").contains(expectedParkingEndTimeSecond);
            }
            logger.info("Checking parking purchase time.");
            softly.assertThat(expectedTotalTime).as("Total time is not correct!").isEqualTo(actualTotalTime);
            logger.info("Checking parking lot name.");
            softly.assertThat(expectedLotName).as("Lot name is not correct!").isEqualTo(actualLotName);
            logger.info("Checking parking zone.");
            softly.assertThat(expectedZone).as("Zone is not correct!").isEqualTo(actualZone);
            logger.info("Checking parking vehicle plate.");
            softly.assertThat(expectedVehiclePlate).as("Vehicle plate is not correct!").isEqualTo(actualVehicle);
            logger.info("Checking parking purchase number.");
            softly.assertThat(expectedPurchaseNumber).as("Purchase number is not correct!").isEqualTo(actualPurchaseNumber);
            logger.info("Checking parking total amount.");
            softly.assertThat(expectedTotalAmount).as("Total amount is not correct!").isEqualTo(actualTotalAmount);
            logger.info("Checking parking credit card.");
            softly.assertThat(expectedCreditCard).as("Credit card is not correct!").isEqualTo(actualCreditCard);
        });
        logger.info("Extended parking information is correct.");
    }
}
