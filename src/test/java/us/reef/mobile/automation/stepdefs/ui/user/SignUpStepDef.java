package us.reef.mobile.automation.stepdefs.ui.user;

import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.UserEntity;
import us.reef.mobile.automation.ui.pages.registration.CreateYourAccountPage;
import us.reef.mobile.automation.utils.RegisterNewUserUtil;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class SignUpStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private CreateYourAccountPage createYourAccountPage;

    @When("^Enters valid information in sign up form$")
    public void createAccountWithValidData() {
        assertThat(createYourAccountPage.isCreateYourAccountPageDisplayed()).as("Create your account page is not displayed!").isTrue();
        final String email = RegisterNewUserUtil.getRandomEmail();
        final String password = RegisterNewUserUtil.getRandomPassword(12);
        final String mobilePhoneNumber = RegisterNewUserUtil.generateRandomMobilePhoneNumberOnSignUp();

        createYourAccountPage.enterEmail(email);
        createYourAccountPage.enterPassword(password);
        createYourAccountPage.enterMobilePhone(mobilePhoneNumber);

        final UserEntity user = UserEntity.builder()
                .email(email)
                .password(password)
                .mobilePhoneNumber(mobilePhoneNumber)
                .build();
        storage.getUserEntities().add(user);
    }

    @When("^Agrees to License Agreement$")
    public void agreeToLicenseAgreement() {
        createYourAccountPage.checkLicenseAgreementSwitch();
    }

    @When("^Tap(?:s|ped)? on Create Account button$")
    public void tappedCreateAccountButton() {
        createYourAccountPage.tapCreateAccountButton();
    }
}
