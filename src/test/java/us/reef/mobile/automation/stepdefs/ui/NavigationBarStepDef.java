package us.reef.mobile.automation.stepdefs.ui;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.account.AccountPage;
import us.reef.mobile.automation.ui.pages.parking.ParkingSessionPage;
import us.reef.mobile.automation.ui.pages.parking.SearchLotPage;
import us.reef.mobile.automation.ui.pages.paynow.RecentPage;

import static org.assertj.core.api.Assertions.assertThat;

public class NavigationBarStepDef {

    @Autowired
    private HomePage homePage;

    @Autowired
    private AccountPage accountPage;

    @Autowired
    private ParkingSessionPage parkingSessionPage;

    @Autowired
    private RecentPage recentPage;

    @Autowired
    private SearchLotPage searchLotPage;

    @Given("^User navigated to account screen$")
    public void navigateToAccountScreen() {
        homePage.clickAccountButton();
        assertThat(accountPage.isAccountPageDisplayed()).as("Account page is not displayed!").isTrue();
    }

    @When("^(?:Will )?[Nn]avigate to [Mm]ap$")
    public void navigateBackButton() {
        parkingSessionPage.tapMapButton();
    }

    @When("^Navigate to Your Recent Lots screen$")
    public void navigateToRecentLotsScreen() {
        searchLotPage.clickOnPayNowButton();
        homePage.clickRecentLotsButton();
        assertThat(recentPage.isRecentPageDisplayed()).as("Recent screen is not displayed!").isTrue();
    }
}
