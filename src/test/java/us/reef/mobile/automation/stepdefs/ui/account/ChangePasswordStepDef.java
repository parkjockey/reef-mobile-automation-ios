package us.reef.mobile.automation.stepdefs.ui.account;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.rest.proxy.EmailProxy;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.pages.account.ChangePasswordPage;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 26/11/2020
 */
public class ChangePasswordStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private ChangePasswordPage changePassword;

    @Autowired
    private EmailProxy email;

    final static String PASSWORD_RESET_REQUEST_EMAIL_SUBJECT = "REEF Mobile Password Reset Requested [UAT]";

    @Then("^Change password page will be displayed$")
    public void entersResetCodeAndNewPassword() {
        assertThat(changePassword.isChangePasswordPageDisplayed()).as("Change password page is not displayed!")
                .isTrue();
    }

    @Then("^Reset password email will be received with correct content$")
    public void emailReceivedWithCorrectContent() {
        final String userEmail = storage.getLastUserEntity().getEmail();
        final String mobilePhone = storage.getLastUserEntity().getMobilePhoneNumber();
        final String title = "REEF™ Mobile Password Reset UAT";
        final String firstLine = "  You requested a password reset for your REEF™ Mobile account. A";
        final String secondLine = "password reset code has been sent via SMS to your mobile phone " + mobilePhone.substring(0, 5);
        final String thirdLine = mobilePhone.substring(6) + ".";
        final String fourthLine = "If you did not make this request, please check your mobile application or";
        final String fifthLine = "contact our customer support team at 1-888-561-7333.";
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(email.isMailReceivedByReceiver(PASSWORD_RESET_REQUEST_EMAIL_SUBJECT, userEmail))
                    .as("Reset password request email not found!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(title))
                    .as("Email title is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(firstLine))
                    .as("First line is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(secondLine))
                    .as("Second line is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(thirdLine))
                    .as("Third line is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(fourthLine))
                    .as("Fourth line is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(fifthLine))
                    .as("Fifth line is not correct!").isTrue();
        });
    }
}