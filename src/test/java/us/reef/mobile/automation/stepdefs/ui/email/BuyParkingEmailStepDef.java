package us.reef.mobile.automation.stepdefs.ui.email;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.rest.proxy.EmailProxy;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.utils.EmailUtil;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 18/11/2020
 */
public class BuyParkingEmailStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private EmailProxy email;

    final static String PARKING_RECEIPT_EMAIL_SUBJECT = "REEF Mobile Parking Receipt [UAT]";

    @Then("^Parking receipt email is received with correct content$")
    public void parkingReceiptEmail() {
        final String userEmail = storage.getLastUserEntity().getEmail();
        final String title = "REEF™ Mobile Parking Receipt UAT";
        final String purchaseNumber = "Purchase Number: " + storage.getLastParkingEntity().getPurchaseNumber();
        final String accountNumber = "Account: " + storage.getLastUserEntity().getMobilePhoneNumber();
        final String totalAmount = "Total Amount: " + storage.getLastParkingEntity().getTotalParkingAmount();
        final String fees = "Fees: " + storage.getLastParkingEntity().getParkingFee();
        final String parkingStartTimeFirst = EmailUtil.removeInitialZero(storage.getLastParkingEntity().getStartTime());
        final String parkingStartTimeSecond = EmailUtil.removeInitialZero(LocalTime.of(
                ParkingTimeUtil.extractHours(parkingStartTimeFirst),
                ParkingTimeUtil.extractMinutes(parkingStartTimeFirst)
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm")));
        final String parkingEndTimeFirst = EmailUtil.removeInitialZero(storage.getLastParkingEntity().getEndTime());
        final String parkingEndTimeSecond = EmailUtil.removeInitialZero(LocalTime.of(
                ParkingTimeUtil.extractHours(parkingEndTimeFirst),
                ParkingTimeUtil.extractMinutes(parkingEndTimeFirst)
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm")));
        final String lotName = "Lot: " + storage.getLastParkingEntity().getLotName();
        final String vehicle = "Vehicle: " + storage.getLastParkingEntity().getVehicle();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(email.isMailReceivedByReceiver(PARKING_RECEIPT_EMAIL_SUBJECT, userEmail))
                    .as("Parking receipt email not found!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(title))
                    .as("Email title is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(purchaseNumber))
                    .as("Purchase number is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(accountNumber))
                    .as("Account number is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(totalAmount))
                    .as("Total amount is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(fees))
                    .as("Parking fee is not correct!").isTrue();
            if (email.emailHasCorrectContent(parkingStartTimeFirst)) {
                softly.assertThat(email.emailHasCorrectContent(parkingStartTimeFirst))
                        .as("Parking start time is not correct!").isTrue();
            } else {
                softly.assertThat(email.emailHasCorrectContent(parkingStartTimeSecond))
                        .as("Parking start time is not correct!").isTrue();
            }
            if (email.emailHasCorrectContent(parkingEndTimeFirst)) {
                softly.assertThat(email.emailHasCorrectContent(parkingEndTimeFirst))
                        .as("Parking end time is not correct!").isTrue();
            } else {
                softly.assertThat(email.emailHasCorrectContent(parkingEndTimeSecond))
                        .as("Parking end time is not correct!").isTrue();
            }
            softly.assertThat(email.emailHasCorrectContent(lotName))
                    .as("Lot name is not correct!").isTrue();
            softly.assertThat(email.emailHasCorrectContent(vehicle))
                    .as("Vehicle is not correct!").isTrue();
        });
    }
}