package us.reef.mobile.automation.stepdefs.ui.parking;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.pages.parking.BuyParkingPage;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.parking.ExtendParkingPage;
import us.reef.mobile.automation.utils.CreditCardUtil;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.*;

public class ParkingStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private BuyParkingPage buyParkingPage;

    @Autowired
    private ExtendParkingPage extendParkingPage;

    final Logger logger = LoggerFactory.getLogger(ParkingStepDef.class);

    @When("^(?:Will )?[Ss]elects? parking ([Ee]xtension )?duration of (\\d+) hour(?:s)? and (\\d+) minutes$")
    public void selectParkingDuration(final String extension, final int hours, final int minutes) {
        if (extension == null) {
            assertThat(buyParkingPage.isBuyParkingPageDisplayed()).as("Buy parking page is not displayed!").isTrue();
        } else {
            assertThat(extendParkingPage.isExtendParkingPageDisplayed()).as("Extend parking page is not displayed!").isTrue();
        }
        buyParkingPage.enterMinutes(minutes);
        buyParkingPage.enterHours(hours);
        if (hours == 0 && minutes == 0) {
            assertThat(buyParkingPage.isSnackBarMessageDisplayed()).as("Notification message that period of time needs to be selected is nto displayed!")
                    .isTrue();
            assertThat(buyParkingPage.isPleaseTryAgainButtonEnabled())
                    .as("Please try again button is not enabled.").isTrue();
        } else if (extension == null) {
            buyParkingPage.checkThatBuyEndTimeIsCorrect(hours, minutes);
        } else {
            extendParkingPage.checksThatExtendEndTimeIsCorrect(hours, minutes);
        }
    }

    @When("^Buys parking with duration of (\\d+) hour(?:s)? and (\\d+) minutes$")
    public void buyParkingWithDuration(final int hours, final int minutes) {
        selectParkingDuration(null, hours, minutes);
        requestRate(null);
        buysParkingSession();
    }

    @When("^(On [Ee]xtension )?(?:Will )?[Tt]ap(?:s|ped)? Request Rate button$")
    public void requestRate(final String extension) {
        buyParkingPage.clickOnRequestRateButton();
        if (extension == null) {
            assertThat(buyParkingPage.isQuoteFeeCorrect()).as("Quote fee text is not correct!").isTrue();
        }
    }

    @When("^Buys parking session$")
    public void buysParkingSession() {
        buyParkingPage.clickOnBuyNowButton();
        assertThat(buyParkingPage.isConfirmYourVehiclePopUpDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickOnYesButtonParkingPopUpButton();
    }

    @When("^Buys free parking session$")
    public void buysFreeParkingSession() {
        buyParkingPage.clickOnRequestRateButton();
        assertThat(buyParkingPage.isFreeParkingTextDisplayed()).as("Free parking message is not displayed!").isTrue();
        buyParkingPage.clickOnBuyNowButton();
        assertThat(buyParkingPage.isConfirmYourVehiclePopUpDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickOnYesButtonParkingPopUpButton();
    }

    @Then("^Request Rate (?:is|will be)? displayed$")
    public void isRequestRateDisplayed() {
        assertThat(buyParkingPage.isRequestRateButtonDisplayed()).as("Buy Now button is not displayed").isTrue();
    }

    @Then("^Buy [Nn]ow button (?:is|will be)? displayed$")
    public void isBuyNowButtonDisplayed() {
        buyParkingPage.isBuyNowButtonDisplayed();
    }

    @Then("^Information on Extend Parking page will be correct$")
    public void informationOnExtendParkingPageWillBeCorrect() {

        assertThat(extendParkingPage.isExtendParkingPageDisplayed()).as("Extend Parking page is not displayed!").isTrue();

        final ParkingEntity originalParking = storage.getLastParkingEntity();
        final String actualVehiclePlate = extendParkingPage.getVehiclePlate();
        final String actualCreditCard = extendParkingPage.getCreditCard();
        final String actualOriginalSessionTime = extendParkingPage.getOriginalSessionTime();
        final String actualExtendTime = extendParkingPage.getExtendUntilTime();
        final String expectedVehiclePlate = originalParking.getVehicle();
        final String expectedCreditCard = CreditCardUtil.getCreditCardValueForExtensionParkingPage(
                storage.getLastUserEntity().getCreditCards().get(0));
        final String expectedOriginalSelectedTime = originalParking.getSelectedTime();
        final String expectedExtendUntilFirst = ParkingTimeUtil.extendParkingExpectedEndTime(1, 0,0,
                DateTimeFormatter.ofPattern(storage.getMobileEntity().getTimeFormat()));
        final String expectedExtendUntilSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(expectedExtendUntilFirst),
                ParkingTimeUtil.extractMinutes(expectedExtendUntilFirst)
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(actualVehiclePlate)
                    .as("Vehicle plate {} is not same as the original {}",actualVehiclePlate, expectedVehiclePlate)
                    .isEqualTo(expectedVehiclePlate);
            softly.assertThat(actualCreditCard)
                    .as("Credit card {} is not same as the original {}", actualCreditCard, expectedCreditCard)
                    .isEqualTo(expectedCreditCard);
            softly.assertThat(actualOriginalSessionTime)
                    .as("Original session {}, is not same as the original {}", actualOriginalSessionTime, expectedOriginalSelectedTime)
                    .isEqualTo(expectedOriginalSelectedTime);
            if (actualExtendTime.contains(expectedExtendUntilFirst)) {
                softly.assertThat(actualExtendTime)
                        .as("Actual extend time {}, is not same as expected {}", actualExtendTime, expectedExtendUntilFirst)
                        .contains(expectedExtendUntilFirst);
            } else {
                softly.assertThat(actualExtendTime)
                        .as("Actual extend time {}, is not same as expected {}", actualExtendTime, expectedExtendUntilSecond)
                        .contains(expectedExtendUntilSecond);
            }
        });
        final ParkingEntity extendParkingEntity = ParkingEntity.builder()
                .vehicle(expectedVehiclePlate)
                .creditCard(expectedCreditCard)
                .endTime(actualExtendTime)
                .build();
        storage.getParkingEntities().add(extendParkingEntity);
        logger.info("Vehicle plate, credit card, Original session time and Extend Until time are correct.");
    }

    @Then("^Extend [Nn]ow button (?:is|will be)? displayed$")
    public void isExtendNowButtonDisplayed() {
        assertThat(extendParkingPage.isExtendNowButtonEnabled()).as("Extend Now button is not displayed").isTrue();
    }

    @When("^(?:Will )?[Tt]ap(?:s|ed)? Extend Now button$")
    public void extendParking() {
        extendParkingPage.clickExtendNowButton();
    }

    @Then("^Will be navigated to Extend Parking page for the same lot$")
    public void willBeNavigatedToExtendParkingPageForSameLot() {
        final String lotName = storage.getFirstParking().getLotName();
        logger.info("Checking is Extend Parking page displayed.");
        assertThat(extendParkingPage.isExtendParkingPageDisplayed()).as("Extend parking page is not displayed!").isTrue();
        logger.info("Checking is correct lot name displayed.");
        assertThat(extendParkingPage.getLotName()).as("Lot name is not correct!").isEqualTo(lotName);
    }

    @When("^Tap(?:s|ed)? Buy Now button and changes vehicle to (.*)$")
    public void changeVehicleOnBuying(final String vehicle) {
        buyParkingPage.clickOnBuyNowButton();
        assertThat(buyParkingPage.isConfirmYourVehiclePopUpDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickChangeVehiclePopupButton();
        buyParkingPage.selectVehicle(vehicle);
    }
}
