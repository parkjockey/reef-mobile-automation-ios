package us.reef.mobile.automation.stepdefs.ui.parking;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.parking.BuyParkingPage;
import us.reef.mobile.automation.ui.pages.parking.SearchLotPage;

import static org.assertj.core.api.Assertions.assertThat;

public class SearchLotStepDef {

    @Autowired
    private SearchLotPage searchLotPage;

    @Autowired
    private BuyParkingPage buyParkingPage;

    final Logger logger = LoggerFactory.getLogger(SearchLotStepDef.class);

    @Given("^(?:Starts typing|Types full name of valid lot)? (.*) in type name search box$")
    public void typeInTypeNameSearchBox(final String lotName) {
        searchLotPage.enterLotNameInTypeNameSearchBox(lotName.toUpperCase());
    }

    @When("^Searches and selects lot (.*)$")
    public void searchForLot(final String lot) {
        searchLotPage.searchForLot(lot);
    }

    @Given("^Search results will contain (.*) in lot names$")
    public void searchResultsContainCorrectPartialLotName(final String lotName) {
        assertThat(searchLotPage.doResultsContainsCorrectPartialLotName(lotName))
                .as("Not all search results contain partial lot name "+lotName+"").isTrue();
        logger.info("Search results contains partial lot name of {}", lotName);
    }

    @Given("^Select a lot from the list and check that it is displayed in buy parking screen$")
    public void selectFirstLostFromTypeNameListResult() {
        final String firstLotName = searchLotPage.getFirstLotNameFromResultList();
        searchLotPage.clickOnFirstLotFromList();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buyParkingPage.isBuyParkingPageDisplayed())
                    .as("Buy parking page is not displayed!").isTrue();
            softly.assertThat(buyParkingPage.areParkingDurationWheelsDisplayed())
                    .as("Parking duration wheels are not displayed!").isTrue();
            softly.assertThat(buyParkingPage.isCorrectLotDisplayed(firstLotName))
                    .as("Lot name is not correct!").isTrue();
        });
    }

    @When("^(?:Will )?[Tt]ap(?:s|ped)? Navigate Back button$")
    public void navigateBackButton() {
        buyParkingPage.clickOnNavigateBackButton();
    }

    @Then("^Will search for lot which does not exist$")
    public void searchLotWhichDoesNotExist() {
        searchLotPage.enterLotNameInTypeNameSearchBox("XXXXXXX");
        assertThat(searchLotPage.isNoResultsFound("XXXXXXX")).as("Search results on Type Name is not empty!").isTrue();
    }
}
