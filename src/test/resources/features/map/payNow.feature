Feature: Pay Now

  User should be able to search for lots from Pay Now > Type Name tab.
  The list auto-populates a list of lot options based on what's being entered.
  For example typing "Ma" will bring up lots like MacKenzie Bldg., MacLeod Place, Main and McDermot etc.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code

  @ui @critical
  Scenario: Type name (C13424)
    Given Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully
    When Starts typing Ma in type name search box
    And Search results will contain Ma in lot names
    And Select a lot from the list and check that it is displayed in buy parking screen
    And Will tap Navigate Back button
    And Types full name of valid lot 60 Ellis St in type name search box
    And Select a lot from the list and check that it is displayed in buy parking screen
    And Will tap Navigate Back button
    Then Will search for lot which does not exist

  @ui @critical
  Scenario: Recent lots (Active session) (C18607)
    Given On sign in entered license plate Plate123 with state Delaware
    And On sign in added Visa test credit card
    And User is logged in successfully
    And Searches and selects lot 60 Ellis St
    And Buys parking with duration of 2 hour and 15 minutes
    And Parking session will be started
    And Will Navigate to Map
    And Searches and selects lot 811 Peachtree
    And Buys parking with duration of 1 hour and 0 minutes
    And Parking session will be started
    And Will Navigate to Map
    When Navigate to Your Recent Lots screen
    Then Recent Lots will be correct
    And Tap on first lot from the list