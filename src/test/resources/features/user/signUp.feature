Feature: Sign Up

  User should be able to register a new account with:
  - Email, Password, Mobile Phone

  @ui @critical
  Scenario: User Signs Up by adding vehicle and credit card (C12641)
    Given User tapped on sign up button
    When Enters valid information in sign up form
    And Agrees to License Agreement
    And Taps on Create Account button
    And Gets verification code
    And Enters verification code
    And On sign in entered license plate Plate123 with state Delaware
    And On sign up adds Visa test credit card
    Then Will be logged in successfully

  @ui @api
  Scenario: User adds license plate on BE and adds visa credit card on UI
    Given Creates new user in the system
    When Added license plate Plate123 with state DE
    And User taps on login button
    And Signs in with valid credentials
    And Gets verification code
    And Enters verification code
    And On sign in adds Visa test credit card
    Then Will be logged in successfully