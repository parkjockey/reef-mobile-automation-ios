Feature: Change password

  User should be able to change its password from account screen and to receive an email with verification of successful change.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully

  @ui @critical
  Scenario: Change password (email) (C20537)
    Given User navigated to account screen
    When Tapped on Change Password menu item
    Then Change password page will be displayed
    And Reset password email will be received with correct content