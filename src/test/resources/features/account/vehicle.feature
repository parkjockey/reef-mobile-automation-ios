Feature: Vehicle

  User should be able to add new vehicle on Account > Vehicles screen.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully

  @ui @critical
  Scenario: Add vehicle (C12731)
    Given User navigated to account screen
    And Tapped on Vehicles menu item
    When Taps on Add button
    And Adds new vehicle to account with license plate PLATE123 and state MN - Minnesota
    Then Vehicle will be added successfully