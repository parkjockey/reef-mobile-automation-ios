Feature: Credit Card

  User should be able to add new Credit Card on Your Account > Credit Cards.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully

  @ui @critical
  Scenario: Add Credit Card (C12734)
    Given User navigated to account screen
    And Tapped on Credit Cards menu item
    When Taps on Add credit card button
    And Adds Visa test credit card
    Then Credit card will be added successfully