Feature: Get Quote

  User should bee able to get a quote for desired parking for desired duration.
  On change of parking duration user must be able to request a new quote.
  On some lots there is a parking fee included which is shown to the user as a message, yet amount can differ depending on the lot.
  Quote should show fee and cost of a parking session for picked duration.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And On sign in entered license plate Plate123 with state Delaware
    And On sign in added Visa test credit card

  @ui @critical
  Scenario: Get Quote (C13434)
    Given User is logged in successfully
    When Searches and selects lot 60 Ellis St
    Then Will select parking duration of 0 hours and 0 minutes
    And Will select parking duration of 1 hours and 0 minutes
    And Request Rate will be displayed
    And Tapped Request Rate button
    And Buy now button will be displayed
    And Will select parking duration of 1 hour and 45 minutes
    And Request Rate will be displayed
    And Will tap Request Rate button
    And Buy now button will be displayed