Feature: Find and buy parking

  User should be able to search for the lot and buy the parking session.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And On sign in entered license plate Plate123 with state Delaware
    And On sign in added Visa test credit card
    And User is logged in successfully

  @ui @critical @smoke
  Scenario: User finds short term parking by lot name and buys it (C13435,C16988)
    Given Searches and selects lot 60 Ellis St
    When Selects parking duration of 2 hours and 15 minutes
    And Tapped Request Rate button
    And Buys parking session
    Then Parking session will be started
    And Session information after buying will be correct
    And Parking receipt email is received with correct content

  @ui @critical @smoke
  Scenario: Buy parking after changing Vehicle from Confirm your Vehicle Pop up (C14821)
    Given User navigated to account screen
    And Tapped on Vehicles menu item
    And Taps on Add button
    And Adds new vehicle to account with license plate PLATE321 and state AL - Alabama
    And Vehicle will be added successfully
    And Navigates from Vehicle screen to Account screen
    And Navigates from Account screen to Home screen
    And Searches and selects lot 60 Ellis St
    And Tapped Request Rate button
    When Taps Buy Now button and changes vehicle to PLATE321 - AL
    And Taps Request Rate button
    And Buys parking session
    Then Parking session will be started
    And Session information after buying will be correct

  @ui @critical
  Scenario: Buy parking during a free period (C15041)
    Given Searches and selects lot Nantucket Memorial Airport
    When Selects parking duration of 1 hour and 0 minutes
    And Buys free parking session
    Then Parking session will be started
    And Session information after buying will be correct